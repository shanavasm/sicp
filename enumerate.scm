(define (enumerate-interval min max)
  (if (> min max)
      nil
      (cons min (enumerate-interval (+ min 1) max))))

(define (enumerate-tree t)
  (cond ((null? t) nil)
        ((pair? t) (append (enumerate-tree (car t))
                           (enumerate-tree (cdr t))))
        (else (list tree))))
