;; 8 queens puzzle
(define nil '())

(load "enumerate.scm")
(load "accumulate.scm")

(define empty-board nil)

;; get row number of queen
(define (get-row queen)
  (car queen))

;; get column number
(define (get-col queen)
  (cdr queen))

;; not if no queen is placed in same row
;; a cell r1,c1 is diagonal to r2,c2 if r2-r1 = c2-c1
(define (safe? board)
  (let* ((queen (car board))
         (rest (cdr board))
         (row (get-row queen))
         (col (get-col queen)))
    (null? (filter (lambda (q) (or (= (get-row q) row)
                                   (= (abs (- (get-row q) row))
                                      (abs (- (get-col q) col))))) rest))))

(define (adjoin-position row col board)
  (cons (cons row col) board))

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter safe?
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position
                    new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

;; start with empty list. ie, 0 columns are filled,
;; once, k-1 columns are filled, place new queen at all 8 rows and filter safe once
