(load "enumerate.scm")
(load "accumulate.scm")


(define (pairs-up-to n)
  (flatmap (lambda (i) (map (lambda (j) (list i j)) (enumerate-interval 1 i)))
           (enumerate-interval 1 n)))

  ;; (accumulate append ()
  ;;             (map (lambda (i)
  ;;                    (map (lambda (j) (list i j)) (enumerate-interval 1 i)))
  ;;                  (enumerate-interval 1 n))))
