(define (fold-left op initial sequence)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (op result (car rest))
              (cdr rest))))
  (iter initial sequence))

(define fold-right accumulate)


(define (rev-left seq)
  (fold-left (lambda (x y) (cons y x)) nil seq))

(define (rev-right seq)
  (fold-right (lambda (x y) (append y (list x))) () seq))
