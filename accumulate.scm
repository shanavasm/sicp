(define (accumulate op init seq)
  (if (null? seq)
      init
      (op (car seq)
          (accumulate op init (cdr seq)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
      nil
      (cons (accumulate op init (map car seqs))
            (accumulate-n op init (map cdr seqs)))))

(define (flatmap proc seq)
  (accumulate append () (map proc seq)))

(define (filter pred seq)
  (cond ((or (null? seq) (not seq)) nil)
        ((not (pred (car seq))) (filter pred (cdr seq)))
        (else (cons (car seq) (filter pred (cdr seq))))))

(define (remove x seq)
  (filter (lambda (i) (not (eq? x i))) seq))

(define (permutations seq)
  (if (or (null? seq) (not seq))
      (list nil)
      (flatmap (lambda (x)
                 (map (lambda (y) (cons x y))
                      (permutations (remove x seq))))
               seq)))
